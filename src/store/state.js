import { getProjectId } from './storage';

export default {
  isLoading: true,
  hasToken: false,
  selectedProjectId: getProjectId(),
  user: {},
  projects: [],
  issues: [],
  mrsAssignedToMe: [],
  mrsCreatedByMe: [],
  pipelines: [],
};
